```
curl -LO https://github.com/neovim/neovim/releases/latest/download/nvim.appimage

chmod u+x nvim.appimage

./nvim.appimage --appimage-extract

sudo mv squashfs-root /

sudo ln -s /squashfs-root/AppRun /usr/bin/nvim
```

copy init.vim file into `~/.config/nvim` folder


install Plug manager

```
sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \
       https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
```

open nvim

type :PlugInstall
