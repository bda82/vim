"
" install plugins
"
call plug#begin('~/.local/share/nvim/plugged')

" deoplete is an auto-completion plugin designed for Neovim
if has('nvim')
  Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
else
  Plug 'Shougo/deoplete.nvim'
  Plug 'roxma/nvim-yarp'
  Plug 'roxma/vim-hug-neovim-rpc'
endif

Plug 'zchee/deoplete-jedi'

" Status bar plugin: vim-airline
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

" Automatic quote and bracket completion
Plug 'jiangmiao/auto-pairs'

" comment plugin
Plug 'scrooloose/nerdcommenter'

" code autoformat + pip install yapf
Plug 'sbdchd/neoformat'

" Code jump (go-to) plugin
Plug 'davidhalter/jedi-vim'

" File managing and exploration plugin
Plug 'scrooloose/nerdtree'

" Multiple cursor editing plugin
Plug 'terryma/vim-multiple-cursors'

" Highlight your yank area
Plug 'machakann/vim-highlightedyank'

" Code folding plugin
Plug 'tmhedberg/SimpylFold'

" Install themes
Plug 'morhetz/gruvbox'

" tabs
Plug 'lewis6991/gitsigns.nvim' " OPTIONAL: for git status
Plug 'nvim-tree/nvim-web-devicons' " OPTIONAL: for file icons
Plug 'romgrk/barbar.nvim'

" TagBar
Plug 'majutsushi/tagbar'

" Fuzzy find
Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-telescope/telescope.nvim', { 'tag': '0.1.4' }
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}

call plug#end()

"
" other settings
"

" enable `deoplete` at the startup 
let g:deoplete#enable_at_startup = 1

" PYTHON PROVIDERS {{{
if has('macunix')
    " OSX
    let g:python3_host_prog = '/usr/local/bin/python3' " -- Set python 3 provider
    let g:python_host_prog = '/usr/local/bin/python2' " --- Set python 2 provider
elseif has('unix')
    " Ubuntu
    let g:python3_host_prog = '/usr/bin/python3' " -------- Set python 3 provider
    let g:python_host_prog = '/usr/bin/python2' " ---------- Set python 2 provider
elseif has('win32') || has('win64')
    " Window
    endif
" }}}
let g:loaded_perl_provider = 0
let g:loaded_ruby_provider = 0
set clipboard+=unnamedplus

" automatically close the method preview window
autocmd InsertLeave,CompleteDone * if pumvisible() == 0 | pclose | endif

" airline themes
" let g:airline_theme='<theme>' " <theme> is a valid theme name

" Enable alignment
let g:neoformat_basic_format_align = 1

" Enable tab to space conversion
let g:neoformat_basic_format_retab = 1

" Enable trimmming of trailing whitespace
let g:neoformat_basic_format_trim = 1

" disable autocompletion, because we use deoplete for completion
let g:jedi#completions_enabled = 0

" open the go-to function in split, not another buffer
let g:jedi#use_splits_not_buffers = "right"

" set highlight duration time to 1000 ms, i.e., 1 second
hi HighlightedyankRegion cterm=reverse gui=reverse
let g:highlightedyank_highlight_duration = 1000

" How can I open the preview window below the current window?
set splitbelow

" How can I navigate through the auto-completion list with Tab
inoremap <expr><tab> pumvisible() ? "\<c-n>" : "\<tab>"


nnoremap <C-t> :NERDTreeToggle<CR>

colorscheme gruvbox
set background=dark

" tagbar
nnoremap <A-t> :TagbarToggle<CR>

" Move to previous/next
nnoremap <silent>    <A-,> <Cmd>BufferPrevious<CR>
nnoremap <silent>    <A-.> <Cmd>BufferNext<CR>

" Re-order to previous/next
nnoremap <silent>    <A-<> <Cmd>BufferMovePrevious<CR>
nnoremap <silent>    <A->> <Cmd>BufferMoveNext<CR>

" Goto buffer in position...
nnoremap <silent>    <A-1> <Cmd>BufferGoto 1<CR>
nnoremap <silent>    <A-2> <Cmd>BufferGoto 2<CR>
nnoremap <silent>    <A-3> <Cmd>BufferGoto 3<CR>
nnoremap <silent>    <A-4> <Cmd>BufferGoto 4<CR>
nnoremap <silent>    <A-5> <Cmd>BufferGoto 5<CR>
nnoremap <silent>    <A-6> <Cmd>BufferGoto 6<CR>
nnoremap <silent>    <A-7> <Cmd>BufferGoto 7<CR>
nnoremap <silent>    <A-8> <Cmd>BufferGoto 8<CR>
nnoremap <silent>    <A-9> <Cmd>BufferGoto 9<CR>
nnoremap <silent>    <A-0> <Cmd>BufferLast<CR>

" Pin/unpin buffer
nnoremap <silent>    <A-p> <Cmd>BufferPin<CR>

" Close buffer
nnoremap <silent>    <A-c> <Cmd>BufferClose<CR>
" Restore buffer
nnoremap <silent>    <A-s-c> <Cmd>BufferRestore<CR>

" Wipeout buffer
"                          :BufferWipeout
" Close commands
"                          :BufferCloseAllButCurrent
"                          :BufferCloseAllButVisible
"                          :BufferCloseAllButPinned
"                          :BufferCloseAllButCurrentOrPinned
"                          :BufferCloseBuffersLeft
"                          :BufferCloseBuffersRight

" Magic buffer-picking mode
nnoremap <silent> <C-p>    <Cmd>BufferPick<CR>
nnoremap <silent> <C-p>    <Cmd>BufferPickDelete<CR>

" Sort automatically by...
nnoremap <silent> <Space>bb <Cmd>BufferOrderByBufferNumber<CR>
nnoremap <silent> <Space>bd <Cmd>BufferOrderByDirectory<CR>
nnoremap <silent> <Space>bl <Cmd>BufferOrderByLanguage<CR>
nnoremap <silent> <Space>bw <Cmd>BufferOrderByWindowNumber<CR>

nnoremap <silent> <A-c> <Cmd>:NERDCommenterComment<CR>


"
" Keymap
"
" nerdcommenter
" <leader>cc / <leader>cu

" jedi-vim
" <leader>d: go to definition
" K: check documentation of class or method
" <leader>n: show the usage of a name in current file
" <leader>r: rename a name

" nerdtree
" C-t

" tagbar 
" A-t

" multicursor
" C-n, C-x than precc `c` to go into `insert` mode

" simple fold
" zo： Open fold in current cursor position
" zO： Open fold and sub-fold in current cursor position recursively
" zc： Close the fold in current cursor position
" zC： Close the fold and sub-fold in current cursor position recursively


" https://jdhao.github.io/2018/12/24/centos_nvim_install_use_guide_en/